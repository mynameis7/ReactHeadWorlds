import * as DataSource from 'nedb'
import { EntityLinkState } from '../components/entity-links';
import {join} from 'path';
import { Entity } from './models/Entity';
import { DEFAULT_PATH } from './Constants';

export interface IHeadWorldsEntityDataSource {
    SearchEntitiesByName(name: string): Promise<Entity[]>;
    GetEntityInfo(id: number): Promise<Entity>;
    GetEntityLink(id: number): Promise<EntityLinkState>;
    SetEntityInfo(id: number, entity: Entity) : Promise<Entity>;
    AddNewEntity(entity: Entity) : Promise<Entity>;
    SetEntityDataSourceRoot(path: string) : void;
}
let data: IHeadWorldsEntityDataSource;
export function GetDataSource() : IHeadWorldsEntityDataSource {
    return data == null ? (data = new HeadWorldsEntityDataSource(DEFAULT_PATH)) : data;
}

class HeadWorldsEntityDataSource implements IHeadWorldsEntityDataSource {
    private d: DataSource;
    constructor(dataSourceRoot: string) {
        this.SetEntityDataSourceRoot(dataSourceRoot);

        
        this.SearchEntitiesByName = this.SearchEntitiesByName.bind(this);
        this.GetEntityInfo = this.GetEntityInfo.bind(this);
        this.GetEntityLink = this.GetEntityLink.bind(this);
        this.SetEntityInfo = this.SetEntityInfo.bind(this);
        this.AddNewEntity = this.AddNewEntity.bind(this);
    }
    async SearchEntitiesByName(name: string): Promise<Entity[]> {
        return new Promise<Entity[]>((resolve, reject) => {
            this.d.find<Entity>({$where : function() { return this.name.toLowerCase().indexOf(name.toLowerCase()) >= 0; } }, (err, docs) => {
                if(err) return reject(err);
                return resolve(docs);
            })
        });
    }
    
    async GetEntityInfo(id: number): Promise<Entity> {
        return new Promise<Entity>((resolve, reject) => {
            this.d.findOne<Entity>({_id: id}, (err, doc) => {
                if(err) return reject(err);
                return resolve(doc);
           });
        })
    }
    
    async GetEntityLink(id: number): Promise<EntityLinkState> {
        return new Promise<EntityLinkState>((resolve, reject) => {
            this.d.findOne({_id: id}, {name: 1}, (err, doc) => {
                if(err) return reject(err);
                return resolve(doc as any);
            })
        });
    }
    
    async SetEntityInfo(id: number, entity: Entity) {
        return new Promise<Entity>((resolve, reject) => {
            this.d.update({_id: id}, entity, {}, (err, doc) => {
                if(err) return reject(err);
                return resolve(doc as any);
            })
        })
    }
    
    async AddNewEntity(entity: Entity) {
        entity._id = undefined;
        return new Promise<Entity>((resolve, reject) => {
            this.d.insert(entity, (err, doc) => {
                if(err) return reject(err);
                return resolve(doc);
            });
        });
    }
    
    SetEntityDataSourceRoot(path: string) : void{
        let dsOptions: DataSource.DataStoreOptions = {filename: join(path, 'entities.jsonl'), autoload: true}
        this.d = new DataSource(dsOptions);
    }
}