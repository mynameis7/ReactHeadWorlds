import {join} from 'path';
import * as DataSource from 'nedb'
import { EntityImage } from './models/EntityImage';
import { ImageType } from './models/ImageTypes';
import { DEFAULT_PATH } from './Constants';

export interface IHeadWorldsImageDataSource {
    GetEntityProfileImagePath(id: number) : Promise<string>;
    GetEntityThumbnailImagePath(id: number) : Promise<string>;
    SetImageDataSourceRoot(path: string) : void;
}
let data : HeadWorldsDataSource = null;
export function GetDataSource() : IHeadWorldsImageDataSource {
    return data == null ? (data = new HeadWorldsDataSource(DEFAULT_PATH)) : data;
}

class HeadWorldsDataSource implements IHeadWorldsImageDataSource {
    private d: DataSource;
    constructor(dataSourceRoot: string) {
        this.SetImageDataSourceRoot(dataSourceRoot);

        this.GetEntityProfileImagePath = this.GetEntityProfileImagePath.bind(this);
        this.GetEntityThumbnailImagePath = this.GetEntityThumbnailImagePath.bind(this);
    }

    async GetEntityProfileImagePath(id: number) {
        return new Promise<string>((resolve, reject) => {
            this.d.findOne<EntityImage>({id: id, imageType: ImageType.Profile}, (err, doc) => {
                if(err) return reject(err);
                return resolve(doc.path);
            })
        })
    }
    async GetEntityThumbnailImagePath(id: number) : Promise<string>{
        return new Promise<string>((resolve, reject) => {
            this.d.findOne<EntityImage>({id: id, imageType: ImageType.Thumbnail}, (err, doc) => {
                if(err) return reject(err);
                return resolve(doc.path);
            })
        })
    }

    SetImageDataSourceRoot(path: string) {
        let dsOptions: DataSource.DataStoreOptions = {filename: join(path, 'images.jsonl'), autoload: true}
        this.d = new DataSource(dsOptions);
    }
}
