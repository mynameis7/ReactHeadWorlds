import { EntityType } from "./EntityTypes";

export interface Entity {
    _id: number,
    name: string,
    type: EntityType,
    description: string,
    linkIds: number[],
}