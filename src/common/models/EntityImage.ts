import { ImageType } from "./ImageTypes";

export interface EntityImage {
    id: number,
    path: string,
    imageType: ImageType
}