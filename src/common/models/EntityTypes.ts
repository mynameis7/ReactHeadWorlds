export enum EntityType {
    World,
    Species,
    Character,
    Civilization,
    Item,
    Timeline
}

export const EntityTypeDescriptions = new Map<EntityType, string>([
    [EntityType.World, "World"],
    [EntityType.Species, "Species"],
    [EntityType.Character,"Character"],
    [EntityType.Civilization, "Civilization"],
    [EntityType.Item, "Item"],
    [EntityType.Timeline, "Timeline"]
]);
