import * as React from 'react';
import {EntityType, EntityTypeDescriptions} from '../common/models/EntityTypes'
import {GetDataSource} from '../common/HeadWorldsEntityDataSource'
import { EntityLinks } from './entity-links';
import { Entity } from '../common/models/Entity';
export interface EntityInfoProps {
    entityId: number,
    OnLinkClick: (id: number) => void
}

export class EntityInfo extends React.Component<EntityInfoProps, Entity> {
    constructor(props: EntityInfoProps) {
        super(props);
        this.state = {
            _id: null,
            name: "",
            description: "",
            type: null,
            linkIds : [],
        }
    }

    async componentDidMount(){
        this.setState(await GetDataSource().GetEntityInfo(this.props.entityId));
    }
    
    async componentDidUpdate(prevProps: EntityInfoProps) {
        if(prevProps.entityId != this.props.entityId) {
            this.setState(await GetDataSource().GetEntityInfo(this.props.entityId));
        }
    }

    async setCurrentEntity(id: number) {
        this.setState(await GetDataSource().GetEntityInfo(id));
    }


    render() {
        (window as any)["descriptions"] = EntityTypeDescriptions;
        (window as any)["entity"] = this.state;
        (window as any)["entityTypes"] = EntityType;
        return (
            <div>
                <h1>{this.state.name}</h1>
                <h3>{EntityTypeDescriptions.get(this.state.type)}</h3>
                <p>{this.state.description}</p>
                <div>
                    {getLinks(this.props, this.state)}
                </div>
            </div>
        )
        
        function getLinks(props:EntityInfoProps, state: Entity) {
            let entityLinkStyle = {
                display: "inline-block",
                margin: 2,
                padding: 2
            }
            return state.linkIds.map(id => {
                return (
                    <a style={entityLinkStyle} onClick={e => {props.OnLinkClick(id)} }><EntityLinks entityId={id} /></a>
                )
            });
        }
    }
}