import * as React from 'react';
import {GetDataSource} from '../common/HeadWorldsEntityDataSource'

export interface EntityLinkProps {
    entityId: number
}

export interface EntityLinkState {
    name: string,
}

export class EntityLinks extends React.Component<EntityLinkProps, EntityLinkState> {
    constructor(props: EntityLinkProps) {
        super(props);
        this.state = {
            name: ""
        }
    }

    async componentDidMount() {
        this.setState(await GetDataSource().GetEntityLink(this.props.entityId));
    }

    async componentDidUpdate(prevProps: EntityLinkProps) {
        if(this.props.entityId != prevProps.entityId) {
            this.setState(await GetDataSource().GetEntityLink(this.props.entityId));
        }
    }

    render() {
        return (
            <div>
                {this.state.name}
            </div>
        )
    }
}