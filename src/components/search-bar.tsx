import * as React from 'react';

export interface SearchBarProps<T> {
    searchDataFunc: (searchText: string) => Promise<T[]>,
    searchProperty: string,
    onSearchResultSelect: (searchResults: T) => void,
    debounceTime?: number
}
export interface SearchBarState<T> {
    searchBarText: "",
    searchBarDebounce: any,
    results: T[]
}

export class SearchBar<T> extends React.Component<SearchBarProps<T>, SearchBarState<T>> {
    constructor(props: SearchBarProps<T>) {
        super(props);
        this.state = {
            searchBarText: "",
            searchBarDebounce: null,
            results: []
        }

        this.searchBarUpdate = this.searchBarUpdate.bind(this);
        this.searchBarSubmit = this.searchBarSubmit.bind(this);
        this.debounce = this.debounce.bind(this);
        this.search = this.search.bind(this);
        this.selectResult = this.selectResult.bind(this);
    }

    searchBarUpdate(event:any) {
        this.setState({searchBarText: event.target.value})
        this.debounce();
    }

    searchBarSubmit(event: any) {
        event.preventDefault();
        if(this.state.searchBarDebounce) {
            clearTimeout(this.state.searchBarDebounce);
        }
        this.search();
    }

    render() {
        let inputStyle = {
            padding: "2px",
            width: "100%",
            height: "50px"
        };
        let formStyle = {
            width: "100%",
            height: "50px"
        };
        
        let results = null;
        if(this.state.results) {
            let list = this.state.results.map((result: T) => {
                return (
                    <li style={{width: "100%", background: "white", padding: "3px"}}><a onClick={(e) => {this.selectResult(result)} }>{(result as any)[this.props.searchProperty]}</a></li>
                )
            })
            results = <ul style={{listStyle: "none", position: "absolute", top: "50px", width: "100%", padding:"0"}}>{list}</ul>
        }
        return (
            <div style={{position: "relative"}}>
                <form style={formStyle} onSubmit={this.searchBarSubmit}>
                    <input style={inputStyle} type="text" value={this.state.searchBarText} onChange={this.searchBarUpdate} />
                </form>
                {results}
            </div>
        )
    }


    private debounce() {
        let debounceTime = this.props.debounceTime ? this.props.debounceTime : 300
        if(this.state.searchBarDebounce == null) {
            this.setState({
                searchBarDebounce: setTimeout(this.search, debounceTime)
            })
        } else {
            clearTimeout(this.state.searchBarDebounce);
            this.setState({
                searchBarDebounce: setTimeout(this.search, debounceTime)
            })
        }
    }
    private async search() {
        if(this.state.searchBarText) {
            let _results = await this.props.searchDataFunc(this.state.searchBarText);
            this.setState({results: _results});
        }
        else {
            this.setState({results: null})
        }
    }

    private selectResult(result: T) {
        this.props.onSearchResultSelect(result);
        this.setState({results: null})
    }
}