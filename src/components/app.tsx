import * as React from 'react';
import { GetDataSource, IHeadWorldsEntityDataSource } from '../common/HeadWorldsEntityDataSource'
import { SearchBar } from './search-bar';
import { Entity } from '../common/models/Entity';
import { EntityInfo } from './entity-info';
import { EntityEdit } from './entity-edit';
export interface AppProps {
}

export interface AppState {
    currentEntityId: number,
    editMode: boolean
}

export class App extends React.Component<AppProps, AppState> {
    private dataSource: IHeadWorldsEntityDataSource;
    constructor(props: AppProps) {
        super(props);
        this.state = {
            currentEntityId: null,
            editMode: false
        }

        this.dataSource = GetDataSource();

        this.SetCurrentEntity = this.SetCurrentEntity.bind(this);
        this.SetCurrentEntityId = this.SetCurrentEntityId.bind(this);
        this.toggleEditMode = this.toggleEditMode.bind(this);
        this.addNewEntity = this.addNewEntity.bind(this);
        
    }
    private SetCurrentEntity(entity: Entity) {
        this.setState({currentEntityId: entity._id});
    }
    private SetCurrentEntityId(id: number) {
        this.setState({currentEntityId: id});
    }

    private toggleEditMode() {
        this.setState({editMode: !this.state.editMode})
    }

    private addNewEntity() {
        this.SetCurrentEntityId(null);
        this.setState({editMode: true});
    }
    render() {
        let entityInfo = null;
        if(this.state.currentEntityId) {
            entityInfo = <EntityInfo entityId={this.state.currentEntityId} OnLinkClick={this.SetCurrentEntityId}/>
        }
        let entityDisplay = this.state.editMode ? 
            <EntityEdit entityId={this.state.currentEntityId} onSubmit={this.toggleEditMode} /> :
            <EntityInfo entityId={this.state.currentEntityId} OnLinkClick={this.SetCurrentEntityId}/>;
        let editButton = (this.state.currentEntityId || this.state.currentEntityId === null)? <button onClick={this.toggleEditMode}>{this.state.editMode ? "Save": "Edit"}</button> : null;
        let addButton = (!this.state.editMode) ? <button onClick={this.addNewEntity}>Add</button> : null;
        return (
            <div>
                {addButton}
                <SearchBar<Entity> onSearchResultSelect={(e)=> {this.setState({currentEntityId: e._id})}} searchDataFunc={this.dataSource.SearchEntitiesByName} searchProperty="name" debounceTime={10}/>
                {editButton}
                {entityDisplay}
            </div>
        )
    }
}