import * as React from 'react';
import { Entity } from '../common/models/Entity';
import { GetDataSource } from '../common/HeadWorldsEntityDataSource';
import { EntityType } from '../common/models/EntityTypes';
import { EntityLinks } from './entity-links';


export interface EntityEditProps {
    entityId: number,
    onSubmit?: () => void
}

export class EntityEdit extends React.Component<EntityEditProps, Entity> {
    constructor(props: EntityEditProps) {
        super(props);
        this.state = {
            _id: null,
            name: "",
            type: null,
            description: "",
            linkIds: [],
        }

        this.handleChange = this.handleChange.bind(this);
    }

    async componentDidMount() {
        this.setState(await GetDataSource().GetEntityInfo(this.props.entityId));
    }

    async componentDidUpdate(prevProps: EntityEditProps) {
        if(prevProps.entityId != this.props.entityId) {
            this.setState(await GetDataSource().GetEntityInfo(this.props.entityId));
        }
    }

    async componentWillUnmount() {
        if(!this.props.entityId){
           return GetDataSource().AddNewEntity(this.state);
        }
        else {
           return GetDataSource().SetEntityInfo(this.props.entityId, this.state);
        }
    }

    handleChange(event: any) {
        const target = event.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        const name: any = target.name;
        if(name === "type") {
            value = Number(value);
        }
    
        this.setState({
          [name]: value
        } as any);
    }

    render() {
        const keys = Object.keys(EntityType).filter(k => typeof EntityType[k as any] === "number");
        const entityOptions = keys.map((key: string) => {
            return (
                <option value={EntityType[key as any]}>{key}</option>
            )
        });
        function getLinks(props:EntityEditProps, state: Entity) {
            let entityLinkStyle = {
                display: "inline-block",
                margin: 2,
                padding: 2
            }
            return state.linkIds.map(id => {
                return (
                    <li style={entityLinkStyle}> <span>X</span><span><EntityLinks entityId={id} /></span></li>
                )
            });
        }
        let linkListStyle = {
            listStyle: null as any
        } 
        return (
            <form onSubmit={this.props.onSubmit}>
                <table>
                    <tr>
                        <td><label>Name</label></td>
                        <td><input name="name" type="text" value={this.state.name} onChange={this.handleChange}/></td>
                    </tr>
                    <tr>
                       <td><label>Entity Type</label></td> 
                       <td><select name="type" value={this.state.type} onChange={this.handleChange}>{entityOptions}</select></td>
                    </tr>
                    <tr>
                       <td><label>Description</label></td> 
                       <td><textarea name="description" value={this.state.description} onChange={this.handleChange}></textarea></td>
                    </tr>
                </table>
                <div>
                    <div>Links</div>
                    <ul style={linkListStyle}>
                        {getLinks(this.props, this.state)}
                        <li><a> + Add LInk</a></li>
                    </ul>
                </div>
            </form>
        )
    }
}